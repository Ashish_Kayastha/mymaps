package com.ashish.mymaps;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends ActionBarActivity {

    private GoogleMap mMap;
    private EditText mLocationEditText;
    private static final float DEFAULT_ZOOM = 15;
    private static final double KATHAMNDU_LAT = 27.716667,
            KATHMANDU_LNG = 85.316666;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mLocationEditText = (EditText) findViewById(R.id.locationEditText);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // When the activity resumes, restore the mapview as it was before it was killed
        MapStateManager mapStateManager = new MapStateManager(this);
        CameraPosition cameraPosition = mapStateManager.restoreCameraPosition();
        if (cameraPosition != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            mMap.moveCamera(cameraUpdate);
            mMap.setMapType(mapStateManager.restoreMapType());
        }
        setUpMapIfNeeded();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Save the map states like latitude, longitude when the activity is killed so that it can be restored later
        MapStateManager mapStateManager = new MapStateManager(this);
        mapStateManager.saveMapState(mMap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.normalMapType:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;

            case R.id.satelliteMapType:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;

            case R.id.hybridMapType:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;

            case R.id.terrainMapType:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
       // Place the default mapview when the app starts to the above defined latitude and longitude
       // Shows the view near Narayanhiti in Kathmandu
       getLocation(KATHAMNDU_LAT, KATHMANDU_LNG, DEFAULT_ZOOM);
    }

    private void getLocation(double lat, double lng, float zoom) {
        // Store Latitude and longitude of the given location
        LatLng latLng = new LatLng(lat, lng);

        // Move the mapview to the Latitude, Longitude stored with given Default Zoom
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        mMap.moveCamera(cameraUpdate);
    }

    public void goToLocation(View view) throws IOException {
        String location = mLocationEditText.getText().toString();

        // Check if the value in the editText is null or not
        if (location != null && location.length() != 0) {

            // Get the address of the above typed location and store as a list
            Geocoder geocoder = new Geocoder(this);
            List<Address> addressList = geocoder.getFromLocationName(location, 1);
            Address address = addressList.get(0);

            // Get the latitude and longitude of the address
            double lat = address.getLatitude();
            double lng = address.getLongitude();

            // To show the entered location in the mapview
            getLocation(lat, lng, DEFAULT_ZOOM);

        } else {
            // If the value is null/empty show the error toast message
            Toast.makeText(this, getString(R.string.enter_location_toast_error), Toast.LENGTH_SHORT).show();
        }
    }
}
