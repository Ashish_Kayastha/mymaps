package com.ashish.mymaps;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class MapStateManager {

    // Constant values for SharedPreferences
    private static final String PREFS_NAME = "Map State";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String ZOOM = "zoom";
    private static final String TILT = "tilt";
    private static final String BEARING = "bearing";
    private static final String MAP_TYPE = "map type";
    private SharedPreferences mSharedPreferences;

    public MapStateManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void saveMapState(GoogleMap googleMap) {
        // Get the position (i.e. laititude,longitude,zoom,etc..) of the map
        CameraPosition cameraPosition = googleMap.getCameraPosition();

        // Save all the datas in the map to SharedPreferences
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putFloat(LATITUDE, (float) cameraPosition.target.latitude);
        editor.putFloat(LONGITUDE, (float) cameraPosition.target.longitude);
        editor.putFloat(ZOOM, cameraPosition.zoom);
        editor.putFloat(TILT, cameraPosition.tilt);
        editor.putFloat(BEARING, cameraPosition.bearing);
        editor.putInt(MAP_TYPE, googleMap.getMapType());
        editor.commit();
    }

    public CameraPosition restoreCameraPosition() {
        double latitude = mSharedPreferences.getFloat(LATITUDE, 0);

        // If the latitude value is 0 then return null and skip other process
        if (latitude == 0) {
            return null;
        }

        double longitude = mSharedPreferences.getFloat(LONGITUDE, 0);
        float zoom = mSharedPreferences.getFloat(ZOOM, 0);
        float tilt = mSharedPreferences.getFloat(TILT, 0);
        float bearing = mSharedPreferences.getFloat(BEARING, 0);

        LatLng latLng = new LatLng(latitude, longitude);
        CameraPosition cameraPosition = new CameraPosition(latLng, zoom, tilt, bearing);
        return cameraPosition;
    }

    public int restoreMapType() {
        return mSharedPreferences.getInt(MAP_TYPE, GoogleMap.MAP_TYPE_NORMAL);
    }
}
